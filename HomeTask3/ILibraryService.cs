﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace HomeTask3
{
    [ServiceContract(CallbackContract = typeof(ILibraryCallback), SessionMode = SessionMode.Required)]
    public interface ILibraryService
    {
        [OperationContract]
        [FaultContract(typeof(LibraryFault))]
        void RequestBook(int id);

        [OperationContract]
        void JoinLibrary(Person person);

        [OperationContract(IsInitiating = false)]
        void ReturnBooks(List<Book> books);

        [OperationContract(IsInitiating = false)]
        ICollection<string> GetBooks();

        [OperationContract(IsInitiating = false, IsTerminating = true)]
        ICollection<Book> Apply();

        [OperationContract(IsOneWay = true)]
        void Leave();
    }

    [ServiceContract]
    public interface ILibraryCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnCallback();
    }

    public class LibraryFault
    {
        [DataMember]
        public string FaultText { get; set; }
    }
}
