﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ServiceClient.ServiceReference1;

namespace ServiceClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var person1 = new Person() { FIO = "Иванов И. И.", Id = 1 };

            var instanceContext = new InstanceContext(new LibraryServiceCallback());
            var client = new LibraryServiceClient(instanceContext);
            client.JoinLibrary(person1);
            try
            {
                client.RequestBook(2);
                client.RequestBook(3);
            }
            catch (FaultException<LibraryFault> ex)
            {
                Console.WriteLine(ex.Detail.FaultText);
            }
            var personBooks = client.Apply().ToList();
            personBooks.ForEach(x => Console.WriteLine(x.Name));
            client.Close();
        }
    }

    public class LibraryServiceCallback : ILibraryServiceCallback
    {
        public void OnCallback()
        {
            Console.WriteLine("Завтра!");
        }
    }
}
